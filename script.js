( function() {

    var pprev = 0;
    var prev = 1;
    var current = 1;
    var solution = 0;

    while ( prev < 4000000 ) {

        pprev = prev;
        prev = current;
        current = prev + pprev;
        if ( !( current % 2 ) ) {
            solution += current;
        }
    }

    document.getElementById( 'solution' )
        .innerHTML += solution;

} )();
